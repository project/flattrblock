https://drupal.org/sandbox/discipolo/1991012 --> link to this project
https://drupal.org/node/1991016 --> flattr.module issue
http://developers.flattr.net/button/ --> flattr button docs

1. this module implements the flattrthis widget as a block.
2. install as usual then find and configure it at admin/structure/blocks
3. the flattr module is a flexible solution if you want multiple flattr buttons
   on your site but in the d7 version is not designed with a global block.
4. widgets & socialmedia modules dont contain flattr, but let me know if someone
   manages to integrate it.
   
   TODO:
   Render array in flattrblock_contents()
   config option for button type
   create a noscript fallback that works
   allow changing language
